const {
  merge
} = require('webpack-merge');
const common = require('./webpack.common.js');

console.log('Cargando configuración de desarrollo de webpack')

module.exports = merge(common, {
  mode: 'development',
  devtool: 'inline-source-map',
  // configuración de webpack-dev-server
  devServer: {
    contentBase: './dist',
  },
  module: {
    rules: [{
      // para poder importar archivos css desde index.js
      test: /\.css$/i,
      use: ['style-loader', 'css-loader'],
    }]
  }
});