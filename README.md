Este proyecto es para probar las funcionalidades básicas de [Webpack](https://webpack.js.org/)
siguiendo el [tutorial oficial](https://webpack.js.org/guides/getting-started/).

Algunas librerías usadas:
- loader [css-loader](https://webpack.js.org/loaders/css-loader) para poder import-ar archivos css (y eventualmente [css modules](https://webpack.js.org/loaders/css-loader/#modules))
- loader [style-loader](https://webpack.js.org/loaders/style-loader) para encadenar con el loader anterior y que inserte el css dinámicamente en el HEAD
- loader [babel-loader](https://webpack.js.org/loaders/babel-loader) para compilar el js con Babel
- loader integrado [asset/resource](https://webpack.js.org/guides/asset-management/#loading-images) para cargar imágenes y fonts
- [webpack-dev-server](https://webpack.js.org/guides/development/#using-webpack-dev-server) para servir con servidor local
- [devtool](https://webpack.js.org/configuration/devtool/) para crear source maps para poder debug-ear el código js compilado
- [html-webpack-plugin](https://github.com/jantimon/html-webpack-plugin#writing-your-own-templates) para generar generar el `dist/index.html` a partir de `src/index.html`
- `clean-webpack-plugin` para borrar todos los archivos no cache-ados en cada build
- [webpack-merge](https://webpack.js.org/guides/production/#setup) para partir la configuración en .dev y .prod (la variable `mode` de Webpack [configura varias cosas](https://webpack.js.org/configuration/mode))
- [mini-css-extract-plugin](https://webpack.js.org/plugins/mini-css-extract-plugin/) compila los css como .css y los inserta como <link> en el <head> del index (sino lo cargaría inline dinámicamente vía js)
- [css-minimizer-webpack-plugin](https://webpack.js.org/plugins/css-minimizer-webpack-plugin/) minificador de css; se puede usar con o sin el plugin anterior

Para crear el proyecto:
```
npm init -y
npm i -D webpack@5 webpack-cli@4 webpack-merge webpack-dev-server html-webpack-plugin clean-webpack-plugin mini-css-extract-plugin css-minimizer-webpack-plugin css-loader style-loader babel-loader @babel/core@7 @babel/preset-env@7 @babel/plugin-transform-runtime@7
npm i core-js@3.8 @babel/runtime-corejs3@7
```

Y reemplazar en `package.json` la llave `"main": ...` por ``"private": "true"``

Para compilar con webpack: `npx webpack --config webpack.prod.js`. Mirar los archivos compilados en `dist/`

Para servir servidor local con auto-recarga de cambios: `npx webpack serve --config webpack.dev.js` y navegar a [localhost:8080](localhost:8080)

La configuración de Babel en `babel.config.json` se basa en los aprendizajes de [saius/babel-basic-boilerplate](https://gitlab.com/saius/babel-basic-boilerplate)

_Nota: si surge un warning `[DEP_WEBPACK_COMPILATION_ASSETS] DeprecationWarning: Compilation.assets will be frozen in future, all modifications are deprecated.` es un semi-bug de `html-webpack-plugin` y se puede despejar instalando el paquete alfa de la versión 5, haciendo `npm i -D html-webpack-plugin@5.0.0-alpha.3`_
