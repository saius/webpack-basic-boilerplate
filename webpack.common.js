const path = require("path")

const {
  CleanWebpackPlugin
} = require('clean-webpack-plugin');
// para generar el index.html dinámicamente
const HtmlWebpackPlugin = require('html-webpack-plugin');

console.log('Cargando configuración general de webpack')

module.exports = {
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: '[name].[contenthash:8].js',
    publicPath: '',
  },
  plugins: [
    new CleanWebpackPlugin(),
    // con este hay que tener cuidado de no settear ningún loader de /html/ que pueda interferir
    new HtmlWebpackPlugin({
      template: 'src/index.html'
    })
  ],
  module: {
    rules: [
      // las rules de css son distintas en dev y prod
      {
        // para poder importar imágenes con url() desde css
        test: /\.(png|svg|jpg|jpeg|gif)$/i,
        type: 'asset/resource',
      },
      {
        // para poder importar fonts con url() desde css
        test: /\.(woff|woff2|eot|ttf|otf)$/i,
        type: 'asset/resource',
      },
      {
        // para hacer el código más compatible con navegadores
        test: /\.m?js$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: 'babel-loader'
        }
      }
    ],
  },
  // para mejorar el cacheo - https://webpack.js.org/guides/caching/
  optimization: {
    runtimeChunk: 'single',
    splitChunks: {
      cacheGroups: {
        vendor: {
          test: /[\\/]node_modules[\\/]/,
          name: 'vendors',
          chunks: 'all',
        },
      },
    },
  },
};