import './style.css'
import './dependency.js'

// arrow function que se debería compilar con babel
let a = () => console.log('=> func!')

// esto también
let b = [1];
b.includes(2)